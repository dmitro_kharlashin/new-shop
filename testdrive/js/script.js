$(document).ready(function() {
    countTotal();
    cutText();
});

function cutText() {
    for (var i=0; i < $('.prod_body').length; i++) {
        //$('.prod_caption').css('font-size','14px');
        $('.prod_body').css('font-size','11px');
        $('.prod_caption a')[i].innerHTML = $('.prod_caption a')[i].innerHTML.substr(0,20)+'...';
        $('.prod_body')[i].innerHTML = $('.prod_body')[i].innerHTML.substr(0,40)+'...';
    }
}

$('.addcart').on('click', function(e){
        e.preventDefault();
        var item =  $(this).attr('data-id');
        $.ajax({
            url:'/site/article',
            dataType: 'text',
            data: {'itemId[pid]' : item},
            type:'post',
            success: function(cart) {
                $('#cart_count a').html(cart);
            },
            error: function(error){
                console.log(error);
            }
        });
    }
);

    $('#add_order').on('submit', function(e){
            e.preventDefault();
            var name =  $('input[name = "User[name]"]').val();
            var surname =  $('input[name = "User[surname]"]').val();
            var email = $('input[name = "User[email]"]').val();
            var phone = $('input[name = "User[phone]"]').val();
            var articles =[];
            $('.product_cart_wrapper').each(function(i, item){
                articles[i] =  {id : $(this).attr('id'), number : $(this).find('input[type="number"]').val()};
            });
            $.ajax({
                url:'',
                dataType: 'json',
                data: {'User[name]' : name,
                    'User[surname]' : surname,
                    'User[email]' : email,
                    'User[phone]' : phone,
                    'articles' : articles
                },
                type:'post',
                success: function(json) {
                    if(json.success == true) {
                        $('#page-content').html(json.order);
                        $('#cart_count a').html(json.cart);
                        //document.getElementById('cart_form').reset();
                        return false;
                    }
                },
                error: function(error){
                    console.log(error);
                }
            });
        }
    );
$('#addcomment').on('submit', function(e){
        e.preventDefault();
        var name =  $('input[name = "formData[name]"]').val();
        var email = $('input[name = "formData[email]"]').val();
        var pid = $('input[name = "formData[pid]"]').val();
        var response = $('#response').val();
        $.ajax({
            url:'',
            dataType: 'text',
            data: {'formData[name]' : name,
                'formData[email]' : email,
                'formData[pid]' : pid,
                'formData[response]' : response
            },
            type:'post',
            success: function(data) {
                //if(json.success == true) {
                    $('.comment-wrapper').prepend('<hr>' + data);
                    document.getElementById('addcomment').reset();
                $('#addcomment').css('display', 'none');
                $('#addcart').css('display', 'block');
                $('#form_addcomment').css('display', 'initial');
                    return false;
                //}
            },
            error: function(error){
                console.log('error');
            }
        });
    }
);
    $("#delete_product").on('click', function(e) {
        e.preventDefault(e);
        var pid =[];
        var n = $('.select_product:checked').length;
        for (var i=1;i <= n;i++) {
            pid.unshift($('.select_product:checked').val());
            $('#select_product'+pid[0]).prop('checked', false);
        };
        if(n == 0){
            $('#cart_error').css('color','red').css('padding-right','50%');
            $('#cart_error').html('Не выбрано ни одного продукта  ');
            return;
        }
        else {
            $('#cart_error').html('');
        }
        $.ajax({
            url:'',
            dataType: 'json',
            data: {
                'pid' : pid
            },
            type:'post',
            success: function(json) {
                if(json.success == true){
                    for(var i = 0; i<pid.length; i++){
                        $('#'+pid[i]).remove();
                    }
                    if ($('.select_product').length !== 0) {
                        $('#cart_count a').html(json.cart);
                        countTotal();
                    }
                    else {
                        $('#cart_count a').html(json.cart);
                        $('.thumbnail_info').html('<p>Корзина пустая. Добавьте,пожалуйста товар.</p>');
                    }
                }
                return  false;
            },
            error: function(error){
                console.log(error);
            }
        });
    });

$('input[type="number"]').on('change', function(){
    countTotal();
});

    $("#all_cart").on('click', function() {
        if($('#all_cart').prop('checked')){
            $('.select_product:enabled').prop('checked', true);
        } else {
            $('.select_product:enabled').prop('checked', false);
        }
    });
    $('#form_addcomment').on('click', function(){
        $('#addcomment').css('display', 'block');
        $('#form_addcomment').css('display', 'none');
        $('#addcart').css('display', 'none');
    });

function countTotal() {
    var number = null;
    var price = null;
    var summ = null;
    for (var i  = 0;i < $('.select_product').length;i++) {
        price =  $('.price')[i];
        number = $('.number')[i];
        summ = summ + (number.value*price.innerHTML);
    $('#count').val(parseFloat(summ).toFixed(2));
    }
}

/**
 * Created by dima on 02.10.14.
 */
