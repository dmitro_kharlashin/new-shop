<div class="col-sm-4 col-lg-4 col-md-4">
    <div class="thumbnail">
        <img height='150' width='320' src="<?=$prod['image']?>" alt="<?=$prod['name']?>">
        <div class="caption">
            <h4 class="pull-right"><?=$prod['price']?></h4>
            <br><h4 class="prod_caption"><a href="<?php echo Yii::app()->request->baseUrl; ?>/site/article/<?=$prod['pid']?>"><?=$prod['name']?></a>
            </h4>
            <p class="prod_body"><?=$prod['body']?></p>
            <button class="addcart btn btn-success" data-id="<?=$prod['pid']?>">Добавить в корзину</button>
        </div>
    </div>
</div>