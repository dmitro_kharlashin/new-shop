<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Сайт онлайн-магазина">
    <meta name="author" content="Харлашин Д.В.">

    <title><?php echo CHtml::encode(Yii::app()->name); ?></title>

    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl;?>/css/bootstrap.min.css" />

    <!-- Custom CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/shop-homepage.css" />

    <!-- Custom JS -->
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/jquery-1.11.0.js', CClientScript::POS_END); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl.'/js/bootstrap.min.js', CClientScript::POS_END); ?>
    <?php Yii::app()->clientScript->registerScriptFile  (Yii::app()->baseUrl.'/js/script.js', CClientScript::POS_END); ?>


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo Yii::app()->request->baseUrl; ?>/">Интернет-супермаркет</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li>
                    <a href="<?php echo Yii::app()->request->baseUrl; ?>/site/about">О нас</a>
                </li>
                <li>
                    <a href="<?php echo Yii::app()->request->baseUrl; ?>/site/delivery">Доставка</a>
                </li>
                <li>
                    <a href="<?php echo Yii::app()->request->baseUrl; ?>/site/contacts">Контактная информация</a>
                </li>
                <li id="cart_count">
                    <a href="<?php echo Yii::app()->request->baseUrl; ?>/site/cart">Корзина <?=Basket::model()->getCount()?></a>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>

<!-- Page Content -->
<div class="container" id="page-content">
    <?php echo $content; ?>
</div>
<!-- /.container -->

<div class="container">

    <hr>

    <!-- Footer -->
    <footer>
        <div class="row">
            <div class="col-lg-12">
                <p>Создано по информации сайта <a href="http://xn--e1avhbe0a.com.ua/">СТМ Фуршет</a></p>
                <p>Copyright &copy; Интернет-супермаркет <?php echo date('Y'); ?></p>
            </div>
        </div>
    </footer>

</div>
<!-- /.container -->

</body>

</html>
