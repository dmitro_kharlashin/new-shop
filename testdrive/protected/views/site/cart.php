<!-- Page Content -->
<div class="container">
    <div class="row">
        <div class="col-sm-4 col-lg-4 col-md-4">
            <div class="thumbnail_info">
                <?php if (!empty($articles)) :?>
 <form action="" name="basket" id="add_order">
     <div class="select_all">
         <span id="cart_error"></span>
         <input type="button" id="delete_product" value="Удалить" class="btn btn-success">
         Выбрать все <input type="checkbox" id="all_cart">
     </div>
     <div class="cart_products">
         <?php foreach ($articles as $article) :?>
             <div class="product_cart_wrapper" id="<?=$article['pid'];?>">
                 <div class="product_cart_img_wrapper">
                     <div class="product_cart_img">
                         <img src="/<?=$article['image'];?>"/>
                         <input type="number" name="itemList<?=$article['pid'];?>" class="number" value="1"/> * <span class="price"><?=$article['price'];?></span>
                     </div>
                 </div>
                 <div class="product_cart_description">
                     <p align="center"><?=$article['name'];?></p>
                     <?=$article['body'];?>
                 </div>
                 <div class="product_selection">
                     <label><input type="checkbox" name="select_product" id="select_product<?=$article['pid'];?>" class="select_product" value="<?=$article['pid'];?>"></label>
                 </div>
             </div>
         <?php endforeach; ?>
         Сумма заказа: <input type="text" name="count" id="count" maxlength="5" size="10" disabled value="0.00">
     </div>
     <div class="cart_form" id="cart_form">
         <p>Введите имя: </p><input type="text" id="name" name="User[name]">
         <p>Введите фамилию: </p><input type="text" id="surname" name="User[surname]">
         <p>Введите номер телефона: </p><input type="text" id="phone" name="User[phone]">
         <p>Введите электронный адрес: </p><input type="text" id="email" name="User[email]">
         <p></p><input type="submit" value="Отправить заказ" class="btn btn-success">
     </div>
 </form>
                <?php else :?>
                <p>Корзина пустая. Добавьте,пожалуйста товар.</p>
                <?php endif;?>
            </div>
        </div>
    </div>
</div>



<!-- /.container -->