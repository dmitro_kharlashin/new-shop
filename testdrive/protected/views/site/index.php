<div class="container">

    <div class="row">

        <div class="col-md-9">

            <div class="row carousel-holder">

                <div class="col-md-12">
                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="3"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="4"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="5"></li>
                        </ol>
                        <div class="carousel-inner">
                            <div class="item active">
                                <img class="slide-image" src="images/фуршет.jpg" alt="Сеть супермаркетов Фуршет">
                            </div>
                            <?php foreach($carousel as $slide):?>
                                <?php $this->renderPartial('//partials/_article_logo', array('slide'=>$slide->attributes)); ?>
                            <?php endforeach; ?>
                        </div>
                        <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                        </a>
                        <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                        </a>
                    </div>
                </div>

            </div>

            <div class="row">
                <?php foreach($prods as $prod):?>
                    <?php $this->renderPartial('//partials/_article_thumbnail', array('prod'=>$prod->attributes)); ?>
                <?php endforeach; ?>
            </div>

        </div>

    </div>

</div>



