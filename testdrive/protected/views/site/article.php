
        <div class="row">



            <div class="col-md-9">

                <div class="thumbnail">

                    <img class="img-responsive" height='800' width='300' src='<?php echo Yii::app()->request->baseUrl; ?>/<?=$product->attributes['logo']?>' alt='<?=$product->attributes['name']?>'>
                    <div class="caption-full">
                        <h4 class="pull-right"><?=$product->attributes['price']?></h4>
                        <br><h4><?=$product->attributes['name']?></h4>
                        <p><?=$product->attributes['body']?></p>
                        <div class="text-right">
<!--                            <form action="" name="cart" id="addcart">-->
<!--                                <input type="hidden" name="itemId[pid]" value="--><?//=$product['pid']?><!--">-->
<!--                                <div class="text-right">-->
<!--                                    <a ><input type="submit" name="submit" value="Добавить в корзину" class="btn btn-success" id="addcart"></a>-->
<!--                                </div>-->
<!--                            </form>-->
<button class="addcart btn btn-success" data-id="<?=$product['pid']?>">Добавить в корзину</button>
                        </div>
                    </div>
                </div>

                <div class="well">

                    <div class="text-right">
                        <a class="btn btn-success" id="form_addcomment">Оставить отзыв о товаре</a>
                    </div>

                    <form action="" name="comment" id="addcomment">
                        <p><label for="name">Ваше имя: </label></p><p><input type="text" name="formData[name]" id="name"></p>
                        <p><label for="email">Ваш email: </label></p><p><input type="text" name="formData[email]" id="email"></p>
                        <p><label for="response">Ваш комментарий: </label></p><p><textarea name="formData[response]" id="response" cols="100" rows="5"></textarea></p>
                        <input type="hidden" name="formData[pid]" id="pid" value="<?=$product['pid']?>">
                        <div class="text-right">
                            <a ><input type="submit" name="submit" value="Сохранить отзыв" class="btn btn-success"></a>
                        </div>
                    </form>

                    <div class="comment-wrapper">
                        <?php foreach($responses as $response):?>
                            <?php $this->renderPartial('//partials/_article_response', array('response'=>$response->attributes)); ?>
                        <?php endforeach; ?>

                    </div>

                </div>

            </div>

        </div>


