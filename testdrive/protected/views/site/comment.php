
<div class="row">



    <div class="col-md-9">

        <div class="thumbnail">
            <img class="img-responsive" height='800' width='300' src='<?php echo Yii::app()->request->baseUrl; ?>/<?=$product['logo']?>' alt='<?=$product['name']?>'>
            <div class="caption-full">
                <h4 class="pull-right"><?=$product['price']?></h4>
                <br><h4><?=$product['name']?></h4>
                <p><?=$product['body']?></p>

            </div>
        </div>

        <div class="well">


            <form action="" name="comment" id="addcomment">
                <p><label for="name">Ваше имя: </label></p><p><input type="text" name="formData[name]" id="name"></p>
                <p><label for="email">Ваш email: </label></p><p><input type="text" name="formData[email]" id="email"></p>
                <p><label for="response">Ваш комментарий: </label></p><p><textarea name="formData[response]" id="response" cols="100" rows="5"></textarea></p>
                <input type="hidden" name="formData[pid]" id="pid" value="<?=$product['pid']?>">
<!--                <input type="hidden" name="formData[date]" id="date" value="--><?//=date("Y-m-d H:i:s")?><!--">-->
                <div class="text-right">
                    <a ><input type="submit" name="submit" value="Сохранить отзыв"></a>
                </div>
            </form>
<div class="comment-wrapper">
    <?php foreach($responses as $response):?>
        <?php $this->renderPartial('//partials/_article_response', array('response'=>$response->attributes)); ?>
    <?php endforeach; ?>

</div>


        </div>

    </div>

</div>
