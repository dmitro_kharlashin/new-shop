<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
                    'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
    public function actionIndex()
    {
        $this->layout = 'new_layout';
        $model = new Products();
        $prods = $model->findAll(array('order'=>'pid DESC'));
        $carousel = $model->findAll(array('order'=>'pid DESC','limit'=>'5'));

        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'
        $this->render('index', array('prods'=>$prods,'carousel'=>$carousel));
    }

    public function actionArticle()
    {
        $comment = new Responses();
        if(Yii::app()->request->isPostRequest){
            if (isset($_POST['itemId'])) {
                $basket = new Basket();
                $itemId = Yii::app()->request->getPost('itemId')['pid'];
                $basket->addItem($itemId);
                echo 'Корзина ' . Basket::model()->getCount();
                return;
            };
            if (isset($_POST['formData'])) {
                $formData = Yii::app()->request->getPost('formData');
                $formData['date'] = date("Y-m-d H:i:s");
                $comment->attributes = $formData;
                $comment->save();
                $last_comment_id = $comment->getPrimaryKey();
                $last_comment = $comment->findByPk($last_comment_id);
                $response = $this->renderPartial('//partials/_article_response', array('response'=>$last_comment->attributes));
//                echo json_encode(array('success'=>true,'data'=>$response));
                return;
            };
        }
        $id = Yii::app()->request->getQuery('id');
        $model = new Products();
        $product = $model->findByAttributes(array('pid'=> $id));
        $this->layout = 'new_layout';
        $responses = $comment->findAllByAttributes(array('pid'=> $id),array('order'=>'date DESC'));
//         renders the view file 'protected/views/site/index.php'
//         using the default layout 'protected/views/layouts/main.php'
        $data = array(
            'product' => $product,
            'responses' => $responses
        );
        $this->render('article', $data);
    }

    public function actionAbout()
    {
        $this->layout = 'new_layout';
        $infos =  Info::model()->findAll();
        $about = array(
            'infos' => $infos
        );

        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'


        $this->render('about', $about);
    }

    public function actionDelivery()
    {
        $this->layout = 'new_layout';
        $sales =  Sale::model()->findAll();
        $delivery = array(
            'sales' => $sales
        );

        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'


        $this->render('delivery', $delivery);
    }

    public function actionContacts()
    {
        $this->layout = 'new_layout';
        $contacts = array(
            'infos' => Contacts::model()->findAll()
        );

        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'


        $this->render('contacts', $contacts);
    }

    public function actionCart()
    {
        $data = array();
        if(Yii::app()->request->isPostRequest){
            if (isset($_POST['articles']) && isset($_POST['User'])) {

                $order = new Order();
                $User = Yii::app()->request->getPost('User');
                $User['date']=date("Y-m-d H:i:s");
                $order->attributes = $User;
                $articles = Yii::app()->request->getPost('articles');

                if($order->save()){
                    $order_id = $order-> getPrimaryKey();
                    foreach ($articles as $value) {
                        $order_products = new OrderProducts();
                        $order_products->attributes = array('order_id' => $order_id, 'pid' => $value['id'], 'number' => $value['number']);
                        $order_products ->save();
                    }
                    Basket::model()->clearCart();
                    echo json_encode(array('success' => true, 'order' => 'Ваш заказ успешно отправлен', 'cart' => 'Корзина '.Basket::model()->getCount()));
                }
                else {
                    echo 'К сожалению,Ваш заказ не отправлен,попробуйте позже.';
                }
                return;
            };
            if (isset($_POST['pid'])) {
            $pid = Yii::app()->request->getPost('pid');
            while (list (, $id) = each($pid)) {
                Basket::model()->removeItem($id);
        };
                print json_encode(array('success'=>true,'cart'=>'Корзина '.Basket::model()->getCount()));
                return;
            };
        }
        $this->layout = 'new_layout';

        $data['articles'] =  Basket::model()->getAll();


        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'


        $this->render('cart', $data );
    }

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
        $this->layout = 'new_layout';
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
}